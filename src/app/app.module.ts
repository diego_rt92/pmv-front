import { FileUploadComponent } from './components/shared/file-upload/file-upload.component';
import { ContainerModule } from './components/container/container.module';
import { MenuTopBarComponent } from './components/menu-top-bar/menu-top-bar.component';
import { Constants } from 'src/app/constants';
import { NgbActiveModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './components/footer/footer.component';
import { LoginComponent } from './components/login/login.component';
import { CheckinComponent } from './components/checkin/checkin.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { ContainerComponent } from './components/container/container.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { AlbumModalComponent } from './components/album-modal/album-modal.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ThemeSelectorComponent } from './components/theme-selector/theme-selector.component';
import { MatTreeModule } from '@angular/material/tree';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatCardModule } from '@angular/material/card';
import { RadioTreeComponent } from './components/shared/radio-tree/radio-tree.component';
import { MatChipsModule } from '@angular/material/chips';
import { MatExpansionModule } from '@angular/material/expansion';
import { OfferSettingsComponent } from './components/offer/offer-settings/offer-settings.component';
import { MatSelectModule } from '@angular/material/select';
import { NgxMatDatetimePickerModule, NgxMatTimepickerModule } from '@angular-material-components/datetime-picker';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { NgxMatMomentModule } from '@angular-material-components/moment-adapter';
import { MatNativeDateModule } from '@angular/material/core';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    MenuTopBarComponent,
    LoginComponent,
    CheckinComponent,
    ContainerComponent,
    SidenavComponent,
    AlbumModalComponent,
    ThemeSelectorComponent,
    RadioTreeComponent,
    FileUploadComponent,
    OfferSettingsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatGridListModule,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    MatSidenavModule,
    MatListModule,
    ContainerModule,
    MatDialogModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatTreeModule,
    MatRadioModule,
    MatCardModule,
    MatChipsModule,
    MatExpansionModule,
    MatSelectModule,
    NgxMatDatetimePickerModule,
    // NgxMatTimepickerModule,
    MatDatepickerModule,
    MatNativeDateModule,
    NgxMatMomentModule
  ],
  providers: [
    Constants,
    NgbActiveModal,
    CookieService,
    MatDatepickerModule,
    // NgxMatMomentModule,
  ],
  entryComponents: [
  ],
  bootstrap: [
    AppComponent
  ],
  exports: [
  ]
})
export class AppModule { }
