import { User } from "./user.model";

export class Session {

  public id: number;
  public token: string;
  public start: string;
  public user: User;
  public roles: string

  constructor(data?) {
    this.id = data && data.id || 0;
    this.token = data && data.token || '';
    this.start = data && data.start || '';
    this.user = data && data.user || new User();
    this.roles = data && data.roles || '';
  }

  public toService() {
    const obj: any = new Object;
    obj.id = this.id;
    obj.token = this.token;
    obj.start = this.start;
    obj.user = this.user;
    obj.roles = this.roles;
    return obj;
  }
}
