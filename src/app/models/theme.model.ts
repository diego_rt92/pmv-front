import { ItemableNode } from './../components/shared/model/itemable-node.model';
export interface CheckBoxElement<T> {
    checked: boolean;

    getParent(): T | undefined;
    getChildren(): T[];
    getId(): any;
    getDescription(): any;
}

export class Theme implements ItemableNode<Theme> {

    id: number;
    name: string;
    subThemes: Theme[] = new Array<Theme>();
    parentTheme?: Theme;
    checked: boolean;

    constructor(data?: any) {
        this.checked = data && data.checked || false;
        this.id = data && data.id || null;
        this.name = data && data.name || null;

        if (data && data.subThemes && data.subThemes.length > 0) {
            data.subThemes.forEach(t => this.subThemes.push(new Theme(t)));
        } 
        this.parentTheme = data && data.parentThemes ? new Theme(data.parentTheme) : undefined;

    }

    public getItemableNodes(): Theme[] {
        return this.subThemes;
    }

    public getItemableId(): string | number {
        return this.id;
    }

    public getItemableName(): string {
        return this.name;
    }


}