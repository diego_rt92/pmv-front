export class Login {
  public username: string;
  public password: string;

  constructor(data?) {
    this.username = data && data.username || '';
    this.password = data && data.password || '';
  }

  public toService() {
    const obj: any = new Object;
    obj.username = this.username;
    obj.password = this.password;
    return obj;
  }
}
