/**
 * Offer models for API connection and interface management.
 * 
 */
export class Offer {

}

export abstract class OfferCreate {
    comment: string;
    price: number;
    dateStart: Date;
    idAlbum: number;
}

export class OfferAuctionCreate extends OfferCreate {
    up: boolean;
    minutes: number;
    slice: number;
    priceOut: number;
}

export class OfferDirectCreate extends OfferCreate {
    exclusive: boolean;
    dateEnd: Date;
}