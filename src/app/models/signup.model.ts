export class Signup {

  public username: string;
  public email: string;
  public role: Array<string>;
  public password: string;
  public password2: string;
  public isPremium: boolean;
  public isCompany: boolean;
  public terms: boolean;

  constructor(data?) {
    this.username = data && data.username || '';
    this.email = data && data.email || '';
    this.role = data && data.role || new Array<string>();
    this.password = data && data.password || '';
    this.password2 = data && data.password2 || '';
    this.isPremium = data && data.isPremium || false;
    this.isCompany = data && data.isCompany || false;
    this.terms = data && data.terms || false;
  }

  public toService() {
    const obj: any = new Object;
    obj.username = this.username;
    obj.email = this.email;
    obj.role = this.role;
    obj.password = this.password;
    return obj;
  }
}
