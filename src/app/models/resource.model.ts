import { Theme } from "./theme.model";

/**
 * Resources group.
 */
export class Album {

    title: string;
    description: string;
    links: string;
    theme: Theme;
    resources: Resource[] = [];
    
    
    constructor(data?: any) {
        this.title = data && data.title || null;
        this.description = data && data.description || null;
        this.links = data && data.links || null;
        this.theme = data && data.theme ? new Theme(data.theme) : new Theme();

        if (data && data.resources) {
            
            data.resources.array.forEach(r => {
                this.resources.push(new Resource(r));
            });
        }
    }

    /**
     * Returns required album fields for API creation. 
     */
    public toCreateDto(): any {
        let albumCreate = Object();
        albumCreate.title = this.title;
        albumCreate.description = this.description; 
        albumCreate.links = this.links;
        albumCreate.themeId = this.theme.id;

        albumCreate.geoNamesId = [1];
        return JSON.stringify(albumCreate);
    }
}

/**
 * Resource representation.
 */
export class Resource {

    id: number;
    albumId: number;
    name: string;
    thumbnail: string;
    
    file: File;
    fileData: string;

    constructor(data?: any) {
        this.id = data && data.id || null;
        this.albumId = data && data.albumId || null;
        this.name = data && data.name || null;
        this.thumbnail = data && data.thumbnail || null;
        this.file = data && data.file || null;
        this.fileData = data && data.fileData || null;
    }


    /**
     * File validator.
     * 
     * @param f Choosen file
     * @returns TRUE if valid and not null or undefined
     */
    public isValid(): boolean {  
        if (this.file === undefined || this.file === null) {
        return false;
        }

        switch (this.file.type.split('/')[0]) {
        case 'image': return true; break;
        case 'video': return true; break;
        case 'audio': return true; break;
        default: return false; break;
        }
    }

    /**
     * Compose data for multipart creation.
     * 
     * @returns file data
     */
    public toCreateDto(): any {
        return this.file;
    }

    public toDataSourceUrl(): any {
        let reader = new FileReader();
        
        reader.readAsDataURL(this.file);
        reader.onload = () => {
            return reader.result;
        }
        
    }
}