import * as moment from 'moment';

export class User {

  public id: number;
  public user: string;
  public email: string;
  public password: string;
  public isCompany: string;
  public treatment: string;
  public name: string;
  public surname1: string;
  public surname2: string;
  public company: string;
  public birth: string;
  public streetType: string;
  public address: string;
  public remainingAddress: string;
  public zipCode: string;
  public city: string;
  public province: string;
  public country: string;
  public phone: string;
  public fiscalCountry: string;
  public idFiscal: string;
  public smsNotification: boolean;
  public pushNotification: boolean;
  public emailNotification: boolean;
  public pin: string;
  public account: string;
  public language: string;
  public enabled: boolean;
  public ban: boolean;
  public deleted: boolean;
  public allGeo: boolean;
  public allThemes: boolean;
  public tsStart: string;
  public tsUpdate: string;
  public tsDelete: string;


  constructor(data?) {
    this.id = data && data.id || 0;
    this.user = data && data.user || '';
    this.email = data && data.email || '';
    this.password = data && data.passwoord || '';
    this.isCompany = data && data.isCompany || '';
    this.treatment = data && data.treatment || '';
    this.name = data && data.name || '';
    this.surname1 = data && data.surname1 || '';
    this.surname2 = data && data.surname2 || '';
    this.company = data && data.company || '';
    this.birth = data && data.birth && data.birth !== '' ? moment(data.birth, 'YYYY-MM-DD').format('DD-MM-YYYY') : '';
    this.streetType = data && data.streetType || '';
    this.address = data && data.address || '';
    this.remainingAddress = data && data.remainingAddress || '';
    this.zipCode = data && data.zipCode || '';
    this.city = data && data.city || '';
    this.province = data && data.province || '';
    this.country = data && data.country || '';
    this.phone = data && data.phone || '';
    this.fiscalCountry = data && data.fiscalCountry || '';
    this.idFiscal = data && data.idFiscal || '';
    this.smsNotification = data && data.smsNotification || false;
    this.pushNotification = data && data.pushNotification || false;
    this.emailNotification = data && data.emailNotification || false;
    this.pin = data && data.pin || '';
    this.account = data && data.account || '';
    this.language = data && data.language || '';
    this.enabled = data && data.enabled || false;
    this.ban = data && data.ban || false;
    this.deleted = data && data.deleted || false;
    this.allGeo = data && data.allGeo || false;
    this.allThemes = data && data.allThemes || false;
    this.tsStart = data && data.tsStart || '';
    this.tsUpdate = data && data.tsUpdate || '';
    this.tsDelete = data && data.tsDelete || '';
  }

  public toService() {
    const obj: any = new Object;
    obj.id = this.id;
    obj.user = this.user;
    obj.email = this.email;
    obj.password = this.password;
    obj.isCompany = this.isCompany;
    obj.treatment = this.treatment;
    obj.name = this.name;
    obj.surname1 = this.surname1;
    obj.surname2 = this.surname2;
    obj.company = this.company;
    obj.birth = this.birth;
    obj.streetType = this.streetType;
    obj.address = this.address;
    obj.remainingAddress = this.remainingAddress;
    obj.zipCode = this.zipCode;
    obj.city = this.city;
    obj.province = this.province;
    obj.country = this.country;
    obj.phone = this.phone;
    obj.fiscalCountry = this.fiscalCountry;
    obj.idFiscal = this.idFiscal;
    obj.smsNotification = this.smsNotification;
    obj.pushNotification = this.pushNotification;
    obj.emailNotification = this.emailNotification;
    obj.pin = this.pin;
    obj.account = this.account;
    obj.language = this.language;
    obj.enabled = this.enabled;
    obj.ban = this.ban;
    obj.deleted = this.deleted;
    obj.allGeo = this.allGeo;
    obj.allThemes = this.allThemes;
    obj.tsStart = this.tsStart;
    obj.tsUpdate = this.tsUpdate;
    obj.tsDelete = this.tsDelete;
    return obj;
  }

}
