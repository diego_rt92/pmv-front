import { FormGroup } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';
import { timeStamp } from 'node:console';

@Component({
  selector: 'app-offer-settings',
  templateUrl: './offer-settings.component.html',
  styleUrls: ['./offer-settings.component.scss']
})
export class OfferSettingsComponent implements OnInit {

  @Input() fgRef: FormGroup;

  // Nested Group references
  directGroup: FormGroup;
  auctionGroup: FormGroup;

  // Config params
  publishMinTs: Date;


  constructor() { }

  ngOnInit(): void {
    this.directGroup = this.fgRef.get('offerDirectGroup') as FormGroup;
    this.auctionGroup = this.fgRef.get('offerAuctionGroup') as FormGroup;

    this.publishMinTs = new Date();
  }

}
