import { AuthService } from './../../services/auth.service';
import { Constants } from 'src/app/constants';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu-top-bar',
  templateUrl: './menu-top-bar.component.html',
  styleUrls: ['./menu-top-bar.component.scss']
})
export class MenuTopBarComponent implements OnInit {

  constructor(
    public constants: Constants,
    public authService: AuthService,

  ) { }

  ngOnInit(): void {
  }


}
