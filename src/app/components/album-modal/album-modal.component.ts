import { OfferDirectCreate, OfferCreate, OfferAuctionCreate } from './../../models/offer.model';
import { OfferService } from './../../services/offer.service';
import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Album, Resource } from 'src/app/models/resource.model';
import { Theme } from 'src/app/models/theme.model';
import { AlbumService } from 'src/app/services/album.service';
import { COMMA, ENTER, SPACE } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';

@Component({
  selector: 'app-album-modal',
  templateUrl: './album-modal.component.html',
  styleUrls: ['./album-modal.component.scss']
})
export class AlbumModalComponent implements OnInit {

  /**
   * Album form fields definition.
   */
  form = new FormGroup({
    title: new FormControl('Primer Album subido desde Angular', 
      [Validators.required, Validators.minLength(3), Validators.maxLength(100)]),
    description: new FormControl('Descripción de prueba. Subida desde Angular.', 
      [Validators.required, Validators.minLength(10), Validators.maxLength(3000)]),
    links: new FormControl(),
    themeGroup: new FormGroup({
      theme: new FormControl(null, Validators.required)
    }),
    resources: new FormControl(null, [Validators.required]),
    offerGroup: new FormGroup({
      // Common offer fields
      comment: new FormControl(''),
      price: new FormControl(0.0, [Validators.min(0)]),
      dateStart: new FormControl(new Date()),
      type: new FormControl(), // 0: Direct / 1: Auction
      // Direct offer fields group
      offerDirectGroup: new FormGroup({
        exclusive: new FormControl(false, [Validators.required]),
        dateEnd: new FormControl()
      }),
      // Auction offer fields group
      offerAuctionGroup: new FormGroup({
        up: new FormControl(null, [Validators.required]),
        minutes: new FormControl(60, [Validators.required]),
        slice: new FormControl(),
        priceOut: new FormControl(0.0, [Validators.min(0)]),
      }),
    }),
  });

  // Chips (links) separator config
  links: Array<string> = [];
  separatorKeysCodes: number[] = [ENTER, COMMA, SPACE];

  constructor(@Inject(MAT_DIALOG_DATA) public data: {name: string},
    private albumService: AlbumService,
    private offerService: OfferService) { }
    
  ngOnInit(): void { 
    this.startOfferTypeChangeListener();
  }

  /**
   * API Album creation petition.
   */
  submit(): void {

    // Map form into new Album
    let album = new Album();
    album.title = this.form.controls['title'].value;
    album.description = this.form.controls['description'].value;
    album.links = this.links.join(' ');
    album.theme = new Theme({ id: this.form.controls['themeGroup'].value.theme });
    album.resources = this.form.controls['resources'].value as Array<Resource>;

    this.albumService.createAlbum(album).subscribe(
      res => { 
        alert('Album creado : ' + res.id);

        // Offer composition and HTTP POST.
        this.linkOfferToAlbum(res.id);
    
      },
      err => { alert('Error al crear el album') }
    );
  }

  /**
   * Post album's OFFER settings.
   * 
   * @param albumId existing Album ID
   */
  linkOfferToAlbum(albumId: number) {

    let o: OfferCreate;

    const offerType = this.form.get('offerGroup')?.get('type')?.value;
    console.log('Offertype selected: ' + offerType );

    o = offerType == 0 ? this.composeOfferDirectCreate() : this.composeOfferAuctionCreate();

    // Set common OfferCreate attributes
    o.idAlbum = albumId;
    o.price = this.form.controls['offerGroup.price'].value;
    o.dateStart = this.form.controls['offerGroup.dateStart'].value;
    o.comment = this.form.controls['offerGroup.comment'].value;;

    // API - Create and link offer to album 
    this.offerService.createOffer(o).subscribe(
      res => alert('Oferta ' + res.id +' vinculada al album: ' + albumId),
      err => alert('Error al asignar la oferta al album: ' + albumId)
    );
  }
  
  /**
   * Set form values into new OfferDirectCreate.
   * 
   * @returns OfferDirectCreate instance
   */
  private composeOfferDirectCreate(): OfferDirectCreate {
    const offer = new OfferDirectCreate(); 
    const offerDirectGroup = this.form.get('offerGroup')?.get('offerDirectGroup');
    let dateEnd = new Date();
    dateEnd.setDate(dateEnd.getDate() + 2);
    offer.dateEnd = dateEnd; // TODO - ONLY FOR TESTING
    // (o as OfferDirectCreate).dateEnd = offerDirectGroup?.get('dateEnd')?.value; 
    offer.exclusive = offerDirectGroup?.get('exclusive')?.value;
    return offer;
  }

  /**
   * Set form values into new OfferActionCreate.
   * 
   * @returns OfferAuctionCreate instance
   */
  private composeOfferAuctionCreate(): OfferAuctionCreate {
    const offer = new OfferAuctionCreate(); 
    const offerAuctionGroup = this.form.get('offerGroup')?.get('offerAuctionGroup');
    offer.up = offerAuctionGroup?.get('up')?.value;
    offer.minutes = offerAuctionGroup?.get('minutes')?.value;
    offer.slice = offerAuctionGroup?.get('slice')?.value;
    console.log('priceOut:' + offerAuctionGroup?.get('priceOut')?.value);
    offer.priceOut = offerAuctionGroup?.get('priceOut')?.value;
    return offer;
  }

  /**
   * CHIPS HANDLER: Add new hashtag or URL to list.
   * 
   * @param event Input change
   */
  addLink(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.links.push(value.trim());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

    this.form.controls['links'].setValue(null);
  }

  /**
   * CHIPS HANDLER: Remove hastag or url from list.
   * 
   * @param link hashtag or url to remove
   */
  removeLink(link: string): void {
    const index = this.links.indexOf(link);

    if (index >= 0) {
      this.links.splice(index, 1);
    }
  }

  /**
   * Clear all formcontrol validators into formGroup passed.
   * 
   * @param form FormGroup to clear
   */
  private removeValidators(form: FormGroup) {
    for (const key in form.controls) {
        form.get(key)?.clearValidators();
        form.get(key)?.updateValueAndValidity();
    }
  }

  /**
   * Update form validators depending on OfferType.
   * Defines a Listener for offertype change event.
   */
  private startOfferTypeChangeListener(): void {

    // On offer type change listener
    this.form.get('offerGroup')?.get('type')?.valueChanges.subscribe(type => {

      let offerDirectGroup = this.form.get('offerGroup')?.get('offerDirectGroup') as FormGroup;
      let offerAuctionGroup = this.form.get('offerGroup')?.get('offerAuctionGroup') as FormGroup;
      
      switch(type) {
        case '0': 
          this.removeValidators(offerAuctionGroup);
          // Set Direct validators
          offerDirectGroup.get('exclusive')?.setValidators([Validators.required]);
          offerDirectGroup.get('dateEnd')?.setValidators([]);
        break;
        case '1': 
          this.removeValidators(offerDirectGroup);
          // Set Auction validators
          offerAuctionGroup.get('up')?.setValidators([Validators.required]);
          offerAuctionGroup.get('minutes')?.setValidators([Validators.required]);
          offerAuctionGroup.get('slice')?.setValidators([]);
          offerAuctionGroup.get('priceOut')?.setValidators([]);
        break;
      }

    });
  }
}
