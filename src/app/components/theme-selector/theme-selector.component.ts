import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Theme } from 'src/app/models/theme.model';
import { ThemeService } from 'src/app/services/theme.service';

@Component({
  selector: 'app-theme-selector',
  templateUrl: './theme-selector.component.html',
  styleUrls: ['./theme-selector.component.scss']
})
export class ThemeSelectorComponent implements OnInit, OnChanges {
  
  @Input() fgRef: FormGroup;
  
  @Input() fcName: string;
  
  /**
   * Main info
   */
  public themes = new Array<Theme>();

  private subs = new Subscription();

  public loading = true;
  
  constructor(private themeService: ThemeService) { }

  ngOnChanges(changes: SimpleChanges): void {
    // console.log('Cambios: ' + JSON.stringify(changes.fgGroup));
    // this.fgGroup = changes.fgGroup.currentValue;
  }

  ngOnInit(): void {
    // console.log('Themes selector - formGroup: ' + JSON.stringify(this.fgGroup.get('themeGroup')?.value));
    this.loadAllthemes();
  }

  /**
   * Get all parent themes.
   */
  loadAllthemes() {
    const sub = this.themeService.getThemes().subscribe(
      (res: Array<any>) => {
        res.forEach(t => {
          let themetemp = new Theme(t);
          this.recursiveThemeLoad(themetemp);
          this.themes.push(themetemp)
        })
        this.themes = Object.assign(Array<Theme>(), this.themes);
        this.loading = false;
      },
      err => { console.log('Error al obtener temas'); this.loading = false;}
    );
    this.subs.add(sub);
  }

  /**
   * Get all parent's subthemes.
   * @param t parent theme
   */
  recursiveThemeLoad(t: Theme) {
    const sub = this.themeService.getSubThemes(t.id).subscribe(
      (res: Array<any>) => {
        t.subThemes = new Array<Theme>();
        res.forEach(st => {
          let sst = new Theme(st)
          this.recursiveThemeLoad(sst);
          t.subThemes.push(sst as Theme)
        });
      },
      err => { console.log('Subtheme error'); this.loading = false}
    );
    this.subs.add(sub);
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}