import { Login } from './../../models/login.model';
import { AuthService } from './../../services/auth.service';
import { Constants } from 'src/app/constants';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit{

  public login: Login = new Login();

  public formLogin = new FormGroup(
    {
      emailUser: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    }
  );

  constructor(
    public constants: Constants,
    public authService: AuthService,
  ) { }

  ngOnInit(): void {

  }

  authenticateUser(): void {
    this.login.username = this.formLogin.controls['emailUser'].value;
    this.login.password = this.formLogin.controls['password'].value;

    //const password = CryptoJS.AES.encrypt(this.login.password, "clavecifrado").toString();
    //this.login.password = password;

    this.authService.authenticateUser(this.login);
  }

  disabled(): boolean {
    if (this.formLogin.controls['emailUser'].valid && this.formLogin.controls['password'].valid) {
      return false;
    } else {
      return true;
    }
  }

  reset(): void {
    this.login = new Login();
    this.formLogin = new FormGroup(
      {
        emailUser: new FormControl('', Validators.required),
        password: new FormControl('', Validators.required)
      }
    );
  }
}
