import { ItemableNode } from '../model/itemable-node.model';
import { NestedTreeControl } from '@angular/cdk/tree';
import { Component, Input, OnInit, SimpleChanges, OnChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatTreeNestedDataSource } from '@angular/material/tree';

@Component({
  selector: 'app-radio-tree',
  templateUrl: './radio-tree.component.html',
  styleUrls: ['./radio-tree.component.scss']
})
export class RadioTreeComponent implements OnInit, OnChanges {

  @Input() fgRef: FormGroup;
  @Input() fcName: string;

  @Input() data: Array<ItemableNode<any>> = new Array<ItemableNode<any>>();
  
  /**
   * Tree configuration
   */
  treeControl = new NestedTreeControl<ItemableNode<any>>(node => node.getItemableNodes());
  dataSource = new MatTreeNestedDataSource<ItemableNode<any>>();
  hasChild = (_: number, node: any) => !!node.getItemableNodes() && node.getItemableNodes().length > 0;

  constructor() { 
    this.dataSource.data = this.data; 
  }
  
  ngOnInit(): void { }

  ngOnChanges(changes: SimpleChanges): void {
    this.dataSource.data = changes.data.currentValue;
    // this.fgRadio = changes['fgTheme']?.currentValue;
    // console.log('Changes!: ' + JSON.stringify(changes.data));
  }

  

}
