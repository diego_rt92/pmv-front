import { logging } from 'protractor';
import { Component, ElementRef, HostListener, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Resource } from 'src/app/models/resource.model';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: FileUploadComponent,
      multi: true
    }
  ]
})
export class FileUploadComponent implements ControlValueAccessor  {

  onChange: Function;

  resources: Array<Resource> = new Array<Resource>();

  constructor(private host: ElementRef<HTMLInputElement>,
    private dom: DomSanitizer) { }

  sanitize(data: any) { 
    return this.dom.bypassSecurityTrustUrl(data);
  }

  writeValue( value: null ) {
    // console.log('FILEUPLOAD - writeValue: ' + value);
    // clear file input
    this.host.nativeElement.value = '';
    this.resources = [];
  }

  registerOnChange( fn: Function ) {
    // console.log('FILEUPLOAD - writeValue: ' + fn);
    this.onChange = fn;
  }

  registerOnTouched( fn: Function ) {
    // console.log('FILEUPLOAD - registerOnTouched: ' + fn);
  }

  /**
   * Choosed file in file-input is added to formControl value.
   * 
   * @param e file input click event
   */
  changeFiles(e: any): void {
    const reader = new FileReader();    
    const file = e.target.files[0];

    if (file != undefined && file !== null) {

      reader.readAsDataURL(e.target.files[0]);
      reader.onload = () => {
        // Map File to Resource
        let resource = new Resource();
        resource.fileData = reader.result as string;
        resource.name = e.target.files[0].name,
        resource['type'] = e.target.files[0].type;
        resource.file = e.target.files[0] as File;

        if (resource.isValid()) {
          // Load resource into resource list
          this.resources.push(resource);
  
          // Register formControl change
          this.onChange(this.resources);
        } else {
          alert('El archivo no es válido. Solo se permiten imágenes, videos o audios');
        }
      };  
    }
  }

  /**
   * Remove Resource from Resource list and update formControl value.
   * 
   * @param resourceName Resource name
   */
  removeResource(resourceName: string): void {
    this.resources = this.resources.filter(r => r.name !== resourceName);
    // Register formControl change
    this.onChange(this.resources);
  }
 
}
