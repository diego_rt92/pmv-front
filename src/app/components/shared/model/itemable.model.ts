/**
 * Item representation.
 * 
 * Useful for combo, checkbox and radiobutton manage. 
 */
export interface Itemable {
    
    getItemableId(): number | string;
    getItemableName(): string;
}