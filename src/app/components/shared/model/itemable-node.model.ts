import { Itemable } from './itemable.model';
/**
 * This interface makes model treateable by radio-tree.
 */
export interface ItemableNode<T> extends Itemable {

    /**
     * Child nodes.
     */
    getItemableNodes(): T[];


}