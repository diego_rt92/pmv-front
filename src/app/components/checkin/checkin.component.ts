import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from './../../services/auth.service';
import { Signup } from './../../models/signup.model';
import { Constants } from 'src/app/constants';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-checkin',
  templateUrl: './checkin.component.html',
  styleUrls: ['./checkin.component.scss']
})
export class CheckinComponent implements OnInit, OnDestroy {

  public signup: Signup = new Signup();
  public errorSignup: boolean = false;
  public signupOk: boolean = false;
  private subcriptions: Subscription = new Subscription();

  public formSignup = new FormGroup(
    {
      email: new FormControl('', [Validators.required, Validators.email, Validators.maxLength(50)]),
      username: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(20)]),
      password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(40)]),
      password2: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(40)]),
      isPremium: new FormControl(false),
      isCompany: new FormControl(false),
      terms: new FormControl(false, Validators.required),
    }
  );

  constructor(
    public constants: Constants,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.subcriptions.unsubscribe();
  }

  registerUser(): void {
    this.signup.email = this.formSignup.controls['email'].value;
    this.signup.username = this.formSignup.controls['username'].value;
    this.signup.password = this.formSignup.controls['password'].value;
    this.signup.password2 = this.formSignup.controls['password2'].value;
    this.signup.isPremium = this.formSignup.controls['isPremium'].value;
    this.signup.isCompany = this.formSignup.controls['isCompany'].value;
    this.signup.terms = this.formSignup.controls['terms'].value;

    console.log(this.formSignup.controls['terms'].value);

    //const password = CryptoJS.AES.encrypt(this.signup.password, "clavecifrado").toString();
    //this.signup.password = password;

    if (this.signup.isPremium) {
      this.signup.role.push('ROLE_PREMIUM');
    }
    const sus = this.authService.registerUser(this.signup).subscribe(
      (ok) => {
        this.errorSignup = true;
        this.signupOk = true;
      },
      (ko) => {
        this.errorSignup = true;
      }
    );
    this.subcriptions.add(sus);
  }

  activate(): void {
    const sus = this.authService.activateAccount(this.signup.email).subscribe();
    this.subcriptions.add(sus);
  }

  disabled(): boolean {
    if (this.formSignup.controls['email'].valid &&
      this.formSignup.controls['username'].valid &&
      this.formSignup.controls['password'].valid &&
      this.formSignup.controls['password2'].valid &&
      this.formSignup.controls['terms'].value === true &&
      this.formSignup.controls['password'].value === this.formSignup.controls['password2'].value) {
      return false;
    } else {
      return true;
    }
  }

  reset(): void {
    if (this.errorSignup) {
      this.errorSignup = false;
    }
    if (this.signupOk) {
      this.signupOk = false;
    }
    this.signup = new Signup();
    this.formSignup = new FormGroup(
      {
        email: new FormControl('', [Validators.required, Validators.email, Validators.maxLength(50)]),
        username: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(20)]),
        password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(40)]),
        password2: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(40)]),
        isPremium: new FormControl(false),
        isCompany: new FormControl(false),
        terms: new FormControl(false, Validators.required),
      }
    );
  }
}

