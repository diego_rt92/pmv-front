export class Constants {

    // Errores
    public readonly ERROR = 'Error';
    public readonly REQUIRED = 'Campo requerido o no válido ';

    // Barra de acción
    public readonly BUTTON_RETURN = 'Volver';
    public readonly BUTTON_ACCEPT = 'Aceptar';
    public readonly BUTTON_CANCEL = 'Cancelar';
    public readonly BUTTON_RESEND = 'Reenviar';

    // Inicio sesión
    public readonly LOG_IN = 'Iniciar sesión';
    public readonly LOG_OUT = 'Cerrar sesión';
    public readonly EMAIL_USER = 'Email o nombre de Usuario';
    public readonly PASS = 'Contraseña';
    public readonly FORGOTTEN_USER = '¿Has olvidado tu nombre de usuario y tu email?';
    public readonly FORGOTTEN_PASS = '¿Has olvidado tú contraseña?';
    public readonly REMEMBER_ME = 'Recuérdame';
    public readonly CREATE_ACCOUNT = 'Crear cuenta';
    public readonly BEGINNING = 'Inicio';
    public readonly ERROR_LOGIN = 'Email o nombre de usuario, o contraseña erróneos.  ';

    //Registro y activación
    public readonly CHECK_IN = 'Registrarse';
    public readonly ACTIVATE_USER = 'Activar cuenta';
    public readonly EMAIL = 'Correo electrónico';
    public readonly NAME_USER = 'Nombre de usuario';
    public readonly CONFIRM_PASS = 'Confirmar contraseña';
    public readonly CHECK_COMPANY = 'Soy una empresa';
    public readonly CHECK_PREMIUM = 'Quiero ser cliente premium ( 5,99€/mes )';
    public readonly TERMS = 'He leído, entiendo y acepto las condiciones generales, reglas, política de privacidad, política de cookies y las políticas relacionadas con el proceso de identificación';
    public readonly THANKYOU = '¡Gracias por registrarte! Por favor, haz clic en el enlace de activación que acabamos de enviarte por email. Si no encuentras el mensaje, revisa las carpetas de spam de tu cliente de correo o reenvíe pulsando el botón Reenviar. ';
    public readonly DIFFERENT_PASSWORDS = 'Las contraseñas no coinciden';

    //Album
    public readonly NEW_ALBUM = 'Nuevo Album';

    // tslint:disable-next-line:one-line
    constructor() { }
}
