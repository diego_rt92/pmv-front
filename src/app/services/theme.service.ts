import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Theme } from '../models/theme.model';
import { AuthService } from './auth.service';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class ThemeService extends BaseService {

  private urlApi = `${environment.baseURL}/theme/`;

  themes: Array<Theme>;
  
  constructor(private httpClient: HttpClient,
    private authService: AuthService) {
    super(httpClient);
  }

  /**
   * Get all themes.
   */
  public getThemes(): Observable<any> {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${this.authService.getToken()}`);
    return this.getRequestCustom(this.urlApi, headers);
  }

  /**
   * Get all subtemes.
   */
     public getSubThemes(themeId: number): Observable<any> {
      const headers = new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.authService.getToken()}`);
      return this.getRequestCustom(`${this.urlApi}${themeId}/sub`, headers);
    }
}
