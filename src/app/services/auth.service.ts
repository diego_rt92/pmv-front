import { CookieService } from 'ngx-cookie-service';
import { Signup } from './../models/signup.model';
import { Observable } from 'rxjs';
import { Login } from './../models/login.model';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Session } from '../models/session.model';
import { Router } from '@angular/router';
import { setTokenSourceMapRange } from 'typescript';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends BaseService {

  private urlApi = `${environment.baseURL}`;

  public errorLogin: boolean = false;
  private session: Session = new Session();

  constructor(
    public http: HttpClient,
    private cookies: CookieService,
    private router: Router
  ) {
    super(http);
  }

  public authenticateUser(login: Login) {
    const url = `${this.urlApi}/auth/signin`;

    return this.postRequest(url, login.toService()).subscribe(
      (ok) => {
        this.session = new Session(ok);
        this.setToken(this.session.token);
        this.router.navigate(['/']);
      },
      (ko) => {
        this.errorLogin = true;
      }
    );
  }

  public registerUser(signup: Signup): Observable<any> {
    const url = `${this.urlApi}/auth/signup`;

    return this.postRequest(url, signup.toService());
  }

  public activateAccount(email: any): Observable<any> {
    const url = `${this.urlApi}/auth/activate`;

    return this.postRequest(url, email);
  }

  public getToken(): string {
    return this.cookies.get("token");
  }

  private setToken(token: string): void {
    this.cookies.set("token", token);
  }

  public logout() {
    this.cookies.delete("token");
  }
}
