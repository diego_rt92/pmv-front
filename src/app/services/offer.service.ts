import { OfferAuctionCreate, OfferDirectCreate, OfferCreate } from './../models/offer.model';
import { BaseService } from './base.service';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AuthService } from './auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OfferService extends BaseService {

  private urlApi = `${environment.baseURL}`;
  
  constructor(private httpClient: HttpClient,
    private authService: AuthService) {
    super(httpClient);
  }

  /**
   * Smart HTTP POST depending Offer instance.
   * 
   * @param offer Concrete offer instance
   * @returns observable
   */
  createOffer(offer: OfferCreate): Observable<any> {
    let url = `${this.urlApi}/`;
    
    switch(true) {
      case offer instanceof OfferDirectCreate: url += 'direct'; break;
      case offer instanceof OfferAuctionCreate: url += 'auction'; break;
      default: throw new Error('Offer instance not managed');
    }
    
    return this.postRequestToken(url, this.authService.getToken(), offer);
  }
  
  // createOfferAuction(offer: OfferAuctionCreate): Observable<any> {
  //   const url = `${this.urlApi}/auction`;
  //   return this.postRequestToken(url, this.authService.getToken(), offer);
  // }

  // createOfferDirect(offer: OfferDirectCreate): Observable<any> {
  //   const url = `${this.urlApi}/direct`;
  //   return this.postRequestToken(url, this.authService.getToken(), offer);
  // }


  /***** PENDIENTE DE MODIFICAR BASESERVICE para evitar esto */
  public postRequest(url: string, body: any): Observable<any> {
    const headers = new HttpHeaders().set('Authorization','Bearer ' + this.authService.getToken());
    return this.http.post(url, body, {headers: headers, withCredentials: true });
  }
}
