import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Album } from '../models/resource.model';
import { KeyValue } from '../models/key-value.model';
import { AuthService } from './auth.service';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class AlbumService extends BaseService {

  private urlApi = `${environment.baseURL}`;
  
  constructor(private httpClient: HttpClient,
    private authService: AuthService) {
    super(httpClient);
  }

  createAlbum(album: Album): Observable<any> {

    let data = new Array<KeyValue>();
    // Add a files key value for resource in Album.
    album.resources.forEach(r => r.file ? data.push(new KeyValue('files', r.toCreateDto())) : null);
    data.push(new KeyValue('album', new Blob([album.toCreateDto()], {type: 'application/json'}) ));

    return this.postMultipartRequest(this.urlApi + '/resources/album', data);
  }

  addResourceToAlbum(resource): void {

    // resource = new File()

    // console.log('Fichero: ' + JSON.stringify(resource) + '; size: ' + resource.size);
    let url = this.urlApi + '/resources/album/2'
    // let formData = new FormData();
    // formData.append('file', resource, resource.name);

    const headers = new HttpHeaders().set('Authorization','Bearer ' + this.authService.getToken());  
    // let nada: any;
    // headers.set('Content-type',nada);

    this.http.put(url, resource, {headers: headers, withCredentials: true }).subscribe(
      res => {
        alert('Add resource OK');
      },
      err => {
        alert('Add resource ERROR');
      }
    )

  }


  private postMultipartRequest(url: string, data: Array<KeyValue>): Observable<any> {
    let formData = new FormData();
    data.forEach(
      e => formData.append(e.key, e.value));

    const headers = new HttpHeaders().set('Authorization','Bearer ' + this.authService.getToken());
  
    return this.http.post(url, formData, {headers: headers, withCredentials: true });
  }

}
