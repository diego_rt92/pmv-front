import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  constructor(
    public http: HttpClient
  ) { }

  public getRequest(url: string): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this.http.get(url, { headers: headers, withCredentials: true });
  }

  public getRequestCustom(url: string, headers: HttpHeaders): Observable<any> {

    return this.http.get(url, { headers: headers, withCredentials: true });
  }

  public postRequest(url: string, params?: any): Observable<any> {
    params = JSON.stringify(params);

    const headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this.http.post(url, params, { headers: headers, withCredentials: true });
  }

  public postRequestToken(url: string, token: string, params?: any): Observable<any> {
    params = JSON.stringify(params);

    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${token}`);

    return this.http.post(url, params, { headers: headers, withCredentials: true });
  }

  public putRequest(url: string, params?: any): Observable<any> {
    params = JSON.stringify(params);

    const headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this.http.put(url, params, { headers: headers, withCredentials: true });
  }

  public deleteRequest(url: string): Observable<any> {
    return this.http.delete(url);
  }
}
